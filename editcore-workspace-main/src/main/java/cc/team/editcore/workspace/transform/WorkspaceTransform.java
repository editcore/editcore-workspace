package cc.team.editcore.workspace.transform;

import cc.team.editcore.network.ECInputStream;
import cc.team.editcore.network.ECOutputStream;
import cc.team.editcore.network.ECSerializable;
import com.sk89q.worldedit.BlockVector;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Workspace transofrm, can be encoded and decoded, and is used to apply a certain transform on a WorkspaceRegion
 */
public abstract class WorkspaceTransform implements ECSerializable {

    public abstract void encode(ECOutputStream stream) throws IOException;

    public abstract void decode(ECInputStream stream) throws IOException;

    public abstract List<BlockVector> apply(BlockVector pos);

    @Override
    public final void encodeInto(ECOutputStream stream) throws IOException {
        TransformBuffer.encodeIn(stream, this);
    }

    @Override
    public final void decodeFrom(ECInputStream stream) throws IOException {
        throw new UnsupportedOperationException();
    }

    public static class TransformBuffer {

        private static Map<Byte, Class<? extends WorkspaceTransform>> registry = new HashMap();

        public static void register(byte id, Class<? extends WorkspaceTransform> clazz){
            if(registry.containsKey(id))
                throw new IllegalArgumentException("Cannot register transform" + clazz + " because the id " + id + " is already taken by " + registry.get(id));
            if(registry.containsValue(clazz))
                throw new IllegalArgumentException("The class " + clazz + " is already registered as a transform");

            registry.put(id, clazz);
        }

        public static void encodeIn(ECOutputStream stream, WorkspaceTransform transform) throws IOException {
            byte id = findID(transform);

            stream.writeByte(id);
            transform.encode(stream);
        }

        public static <T extends WorkspaceTransform> T decodeFrom(ECInputStream stream) throws IOException {
            byte id = stream.readByte();
            try{
                Class<? extends WorkspaceTransform> clazz = registry.get(id);
                T transform = (T) clazz.newInstance();
                transform.decode(stream);
                return transform;
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InstantiationException e) {
                e.printStackTrace();
            }
            return null;
        }

        private static Byte findID(WorkspaceTransform buffer){
            for(Map.Entry<Byte, Class<? extends WorkspaceTransform>> entry : registry.entrySet()){
                if(buffer.getClass().equals(entry.getValue()))
                    return entry.getKey();
            }
            throw new IllegalArgumentException("The transform " + buffer.getClass() + " is not registered");
        }
    }
}
