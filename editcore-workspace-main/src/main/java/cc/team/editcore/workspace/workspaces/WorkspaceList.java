package cc.team.editcore.workspace.workspaces;

import cc.team.editcore.ECPlayer;
import cc.team.editcore.network.ECInputStream;
import cc.team.editcore.network.ECOutputStream;
import cc.team.editcore.network.Sendable;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Store the workspaces of a player
 */
//TODO
public class WorkspaceList implements Sendable {

    //is true if the list is being loaded
    //TODO is it useful ?
    private boolean loading;
    private ECPlayer player;
    private final Map<WorkspaceID, Entry> workspaces = new HashMap<>();

    public WorkspaceList(ECPlayer player) {
        loading = true;
    }

    @Override
    public void encodeInto(ECOutputStream stream) throws IOException {}

    @Override
    public void decodeFrom(ECInputStream stream) throws IOException {
        loading = false;
    }

    public void download() {

    }

    private class Entry {
        WorkspaceID id;
        String name;
        boolean loaded;
    }
}
