package cc.team.editcore.workspace;

import cc.team.editcore.ECPlayer;
import cc.team.editcore.workspace.workspaces.Workspace;
import cc.team.editcore.workspace.workspaces.WorkspaceID;
import cc.team.editcore.workspace.workspaces.LoadedWorkspace;
import cc.team.editcore.workspace.workspaces.WorkspaceList;
import com.boydti.fawe.util.EditSessionBuilder;
import com.boydti.fawe.util.TaskManager;
import com.google.common.base.Preconditions;
import com.sk89q.worldedit.BlockVector;
import com.sk89q.worldedit.EditSession;
import com.sk89q.worldedit.WorldEdit;
import com.sk89q.worldedit.world.World;
import com.sk89q.worldedit.world.block.BlockState;
import com.sk89q.worldedit.world.block.BlockStateHolder;

import java.util.HashMap;
import java.util.Map;

/**
 * Manager for all workspaces, loaded or not.
 */
public class WorkspaceManager {

    //TODO think about reload
    private final Map<ECPlayer, WorkspaceList> lists;
    private final Map<WorkspaceID, LoadedWorkspace> loadedWorkspaces;

    public WorkspaceManager() {
        this.lists = new HashMap<>();
        this.loadedWorkspaces = new HashMap<>();
    }

    public void init(){
        //TODO load loadedWorkspaces from disk
        //TODO load WorkspaceList from connected players (From a reload)
    }

    public void onBlockPlaced(World w, BlockVector pos, BlockStateHolder block, Workspace.BlockPlacement type){
        EditSession session = new EditSessionBuilder(w).allowedRegionsEverywhere().fastmode(true).limitUnlimited().build();
        onBlockPlaced(session, pos, block, type);
        session.flushQueue();
    }

    public void onBlockPlaced(EditSession session, BlockVector pos, BlockStateHolder block, Workspace.BlockPlacement type){
        for(LoadedWorkspace workspace : loadedWorkspaces.values()){
            workspace.onBlockPlaced(session, pos, block, type);
        }
    }

    public void loadPlayerList(ECPlayer player){
        Preconditions.checkState(!this.lists.containsKey(player));

        this.lists.put(player, new WorkspaceList(player));

        TaskManager.IMP.async(() -> this.lists.get(player).download());
    }
}
