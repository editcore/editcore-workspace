package cc.team.editcore.workspace.workspaces;

import com.sk89q.worldedit.BlockVector;
import com.sk89q.worldedit.EditSession;
import com.sk89q.worldedit.Vector;
import com.sk89q.worldedit.world.World;
import com.sk89q.worldedit.world.block.BlockStateHolder;

import java.util.List;

/**
 * A workspace loaded on the server
 */
public class LoadedWorkspace extends Workspace {

    private BlockVector position;
    private World world;

    public LoadedWorkspace(World w, BlockVector pos, WorkspaceID id, List<String> tags, List<ColorRegion> colorRegions, WorkspaceRegion region) {
        super(id, tags, colorRegions, region);
        this.position = pos;
        this.world = w;
    }

    /**
     * Called when a block is placed
     */
    public void onBlockPlaced(EditSession session, BlockVector position, BlockStateHolder block, BlockPlacement type){
        if(isAllowed(type) && this.world.equals(session.getWorld())){
            this.region.dispatcherBlockPlaced(session, position.subtract(this.position).toBlockVector(), block);
        }
    }

    private boolean isAllowed(BlockPlacement type) {
        //TODO
        return true;
    }

    /**
     * Update the workspace, check the changed blocks and update the cloud
     */
    public void update(){

    }
}
