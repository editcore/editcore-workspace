package cc.team.editcore.workspace.workspaces;

import cc.team.editcore.network.ECInputStream;
import cc.team.editcore.network.ECOutputStream;
import cc.team.editcore.network.ECSerializable;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Represent a simple workspace
 */
public class Workspace implements ECSerializable {

    private int currentRevision;

    protected WorkspaceID id = new WorkspaceID();
    protected String name = "";

    protected List<String> tags = new ArrayList<>();
    protected List<ColorRegion> colorRegions = new ArrayList<>();
    protected WorkspaceRegion region = new WorkspaceRegion();

    protected WorkspaceClipboard clipboard = new WorkspaceClipboard(this);

    /**
     * Create a new workspace (or load it from the cloud)
     * @param id this unique id of the workspace
     * @param tags The tags assigned to the workspace
     * @param colorRegions the colored regions of the workspace
     * @param region The region of the workspace
     */
    public Workspace(WorkspaceID id, List<String> tags, List<ColorRegion> colorRegions, WorkspaceRegion region) {
        this.id = id;
        this.tags = tags;
        this.colorRegions = colorRegions;
        this.region = region;
    }

    @Override
    public void encodeInto(ECOutputStream stream) throws IOException {
        id.encodeInto(stream);
        stream.writeString(name);
        stream.writeList(tags, (tag, s) -> s.writeString(tag));
        stream.writeList(colorRegions);
        region.encodeInto(stream);
        clipboard.encodeInto(stream);
    }

    @Override
    public void decodeFrom(ECInputStream stream) throws IOException {
        id.decodeFrom(stream);
        name = stream.readString();
        tags = stream.readList(s -> {return s.readString();});

        colorRegions = stream.readList(s -> {
            ColorRegion color = new ColorRegion();
            color.decodeFrom(s);
            return color;
        });

        region = new WorkspaceRegion();
        region.decodeFrom(stream);
        clipboard = new WorkspaceClipboard(this);
        clipboard.decodeFrom(stream);
    }

    public WorkspaceID getID() {
        return this.id;
    }

    public WorkspaceRegion getRegion() {
        return this.region;
    }

    /**
     * Type of block placement, used to know if the workspace should update itself on specified blocks placement
     */
    public enum BlockPlacement{
        PLAYER,
        WORLDEDIT,
        ALL;
    }
}
