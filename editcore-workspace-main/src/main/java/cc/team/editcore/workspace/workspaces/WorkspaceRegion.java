package cc.team.editcore.workspace.workspaces;

import cc.team.editcore.network.ECInputStream;
import cc.team.editcore.network.ECOutputStream;
import cc.team.editcore.network.ECSerializable;
import cc.team.editcore.workspace.transform.WorkspaceTransform;
import com.boydti.fawe.object.regions.FuzzyRegion;
import com.boydti.fawe.object.regions.PolyhedralRegion;
import com.sk89q.worldedit.BlockVector;
import com.sk89q.worldedit.BlockVector2D;
import com.sk89q.worldedit.EditSession;
import com.sk89q.worldedit.regions.*;
import com.sk89q.worldedit.world.block.BlockStateHolder;

import javax.annotation.Nullable;
import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Spliterator;
import java.util.function.Consumer;

/**
 * A workspace region, store the space where the workspace is, the subregions, the transform aso
 */
public class WorkspaceRegion implements ECSerializable, Iterable<BlockVector> {

    @Nullable
    private WorkspaceRegion parent = null;
    private List<WorkspaceRegion> childs = new LinkedList<>();
    private List<WorkspaceTransform> transforms = new LinkedList<>();

    private Region region;
    private BlockVector origin;

    //TODO check this
    public void dispatcherBlockPlaced(EditSession session, BlockVector position, BlockStateHolder block){
        if(region.contains(position)){
            for(WorkspaceRegion child : childs)
                child.dispatcherBlockPlaced(session, position, block);

            for(WorkspaceTransform transform : this.transforms){
                for(BlockVector newPos : transform.apply(position)){
                    if(region.contains(newPos) && !session.getBlock(newPos).equals(block.toImmutableState())) {
                        //TODO could we use its return value ?
                        session.setBlock(newPos, block);
                        dispatcherBlockPlaced(session, newPos, block);
                    }
                }
            }
        }
    }

    private void setParent(WorkspaceRegion region) {
        this.parent = region;
    }

    @Override
    public void encodeInto(ECOutputStream stream) throws IOException {
        stream.writeList(childs);
        stream.writeList(transforms);
        RegionEncoder.encode(stream, region);
        stream.writeBlockVector(origin);
    }

    @Override
    public void decodeFrom(ECInputStream stream) throws IOException {
        childs = stream.readList(s -> {
            WorkspaceRegion region = new WorkspaceRegion();
            region.decodeFrom(s);
            region.setParent(this);
            return region;
        });

        transforms = stream.readList(s -> {
            return WorkspaceTransform.TransformBuffer.decodeFrom(s);
        });

        region = RegionEncoder.decode(stream);
        origin = stream.readBlockVector();
    }

    public int getWidth() {
        return region.getWidth();
    }

    public int getHeight() {
        return region.getHeight();
    }

    public int getLength() {
        return region.getLength();
    }

    public Region getRegion() {
        return region;
    }

    @Override
    public Iterator<BlockVector> iterator() {
        return region.iterator();
    }

    @Override
    public void forEach(Consumer<? super BlockVector> action) {
        region.forEach(action);
    }

    @Override
    public Spliterator<BlockVector> spliterator() {
        return region.spliterator();
    }

    private static class RegionEncoder{

        public static void encode(ECOutputStream stream, Region region){
            RegionType type = RegionType.getFromClass(region.getClass());
            stream.write((byte) type.ordinal());
            switch(type){
                case CUBOID:
                    CuboidRegion cub = (CuboidRegion) region;
                    stream.writeBlockVector(cub.getMaximumPoint().toBlockPoint());
                    stream.writeBlockVector(cub.getMinimumPoint().toBlockPoint());
                    break;
                case CYL:
                    CylinderRegion cyl = (CylinderRegion) region;
                    stream.writeBlockVector(cyl.getCenter().toBlockPoint());
                    stream.writeBlockVector2D(cyl.getRadius().toBlockVector2D());
                    stream.write((byte) cyl.getMinimumY());
                    stream.write((byte) cyl.getMaximumY());
                    break;
                case ELLIPSOID:
                    EllipsoidRegion elli = (EllipsoidRegion) region;
                    stream.writeBlockVector(elli.getCenter().toBlockPoint());
                    stream.writeBlockVector(elli.getRadius().toBlockPoint());
                    break;
                case POLYGONAL:
                    break;
                case CONVEX:
                    break;
                case FUZZY:
                    break;
                case POLYHEDRAL:
                    break;
                case NULL:
                    break;
            }
        }

        public static Region decode(ECInputStream stream){
            int type = stream.read();
            RegionType regionType = RegionType.values()[type];
            switch(regionType){
                case CUBOID:
                    BlockVector pos1 = stream.readBlockVector();
                    BlockVector pos2 = stream.readBlockVector();
                    return new CuboidRegion(pos1, pos2);
                case CYL:
                    BlockVector center = stream.readBlockVector();
                    BlockVector2D radius = stream.readBlockVector2D();
                    int miny = stream.read();
                    int maxy = stream.read();
                    return new CylinderRegion(center, radius, miny, maxy);
                case ELLIPSOID:
                    center = stream.readBlockVector();
                    BlockVector rad = stream.readBlockVector();
                    return new EllipsoidRegion(center, rad);
                case POLYGONAL:
                    break;
                case CONVEX:
                    break;
                case FUZZY:
                    break;
                case POLYHEDRAL:
                    break;
                case NULL:
                    return new NullRegion();
            }
            return new NullRegion();
        }
    }



    /**
     * Enum with all region types
     */
    public enum RegionType{
        CUBOID(CuboidRegion.class),
        CYL(CylinderRegion.class),
        ELLIPSOID(EllipsoidRegion.class),
        POLYGONAL(Polygonal2DRegion.class),
        CONVEX(ConvexPolyhedralRegion.class),
        FUZZY(FuzzyRegion.class),
        POLYHEDRAL(PolyhedralRegion.class),
        NULL(NullRegion.class);

        Class<? extends Region> region;

        RegionType(Class<? extends Region> clazz) {
            region = clazz;
        }

        public static RegionType getFromClass(Class<? extends Region> clazz){
            for(RegionType type : values())
                if(type.region.equals(clazz))
                    return type;
            return NULL;
        }
    }
}
