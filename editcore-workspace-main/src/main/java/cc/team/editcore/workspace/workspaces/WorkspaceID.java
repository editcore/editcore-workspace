package cc.team.editcore.workspace.workspaces;

import cc.team.editcore.network.ECInputStream;
import cc.team.editcore.network.ECOutputStream;
import cc.team.editcore.network.ECSerializable;
import cc.team.editcore.network.ECKey;

import java.io.IOException;

/**
 * ID of a workspace, defines it. It is how it is registered adn recognised on the cloud.
 */
public final class WorkspaceID implements ECSerializable {

    private static int KEY_SIZE = 8;
    private boolean initialised = false;
    private ECKey id;

    public WorkspaceID(){}

    public WorkspaceID(ECKey id){
        this.id = id;
        initialised = true;
    }

    @Override
    public void encodeInto(ECOutputStream stream) throws IOException {
        stream.writeBytes(id.getCode(), false);
    }

    @Override
    public void decodeFrom(ECInputStream stream) {
        if(initialised)
            throw new IllegalStateException();
        id = new ECKey(stream.readBytes(KEY_SIZE));
        initialised = true;
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return getClass().isInstance(obj) && id.equals(((WorkspaceID) obj).id);
    }

    @Override
    public String toString() {
        return id.toString();
    }
}
