package cc.team.editcore.workspace.workspaces;

import cc.team.editcore.network.ECInputStream;
import cc.team.editcore.network.ECOutputStream;
import cc.team.editcore.network.ECSerializable;
import cc.team.editcore.workspace.ECWorkspace;
import com.boydti.fawe.object.clipboard.DiskOptimizedClipboard;
import com.boydti.fawe.object.clipboard.FaweClipboard;
import com.boydti.fawe.object.schematic.Schematic;
import com.sk89q.jnbt.CompoundTag;
import com.sk89q.worldedit.BlockVector;
import com.sk89q.worldedit.extent.InputExtent;
import com.sk89q.worldedit.extent.clipboard.BlockArrayClipboard;
import com.sk89q.worldedit.extent.clipboard.Clipboard;
import com.sk89q.worldedit.extent.clipboard.io.ClipboardFormat;
import com.sk89q.worldedit.schematic.SchematicFormat;
import com.sk89q.worldedit.world.block.BlockState;
import com.sk89q.worldedit.world.block.BlockTypes;

import java.io.File;
import java.io.IOException;

/**
 * Clipboard, contains the blocks of a workspace. When updated (saved) sends itself to the cloud.
 */
public class WorkspaceClipboard implements ECSerializable {

    private Clipboard blocks;
    private Workspace parent;

    public WorkspaceClipboard(Workspace parent){
        this.parent = parent;
    }

    public WorkspaceClipboard(Workspace parent, Clipboard blocks){
        this.parent = parent;
        this.blocks = blocks;
    }

    public WorkspaceClipboard(Workspace parent, InputExtent extent){
        this.parent = parent;
        WorkspaceRegion reg = parent.getRegion();
        //TODO Check this
        this.blocks = new BlockArrayClipboard(reg.getRegion(), new DiskOptimizedClipboard(reg.getWidth(), reg.getHeight(), reg.getLength(), getFile()));
        for(BlockVector pos : reg){
            blocks.setBlock(pos.getBlockX(), pos.getBlockY(), pos.getBlockZ(), extent.getBlock(pos));
        }
    }

    //TODO make it work across versions
    @Override
    public void encodeInto(ECOutputStream stream) throws IOException {
        stream.writeBlockVector(blocks.getDimensions().toBlockPoint());
        ClipboardFormat.STRUCTURE.getWriter(stream).write(blocks);
    }

    @Override
    public void decodeFrom(ECInputStream stream) throws IOException {
        BlockVector dims = stream.readBlockVector();
        WorkspaceRegion reg = parent.getRegion();
        this.blocks = ClipboardFormat.STRUCTURE.getReader(stream).read();
    }

    private File getFile() {
        return new File(ECWorkspace.getDirectory(), "/clipboards/" + parent.getID().toString());
    }
}
