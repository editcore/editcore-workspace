package cc.team.editcore.workspace.transform;

import cc.team.editcore.network.ECInputStream;
import cc.team.editcore.network.ECOutputStream;
import com.sk89q.worldedit.BlockVector;
import com.sk89q.worldedit.math.transform.AffineTransform;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

public class Rotation extends WorkspaceTransform{

    private double angleX;
    private double angleY;
    private double angleZ;
    private AffineTransform transform;

    public Rotation(double rotX, double rotY, double rotZ) {
        this.angleX = rotX;
        this.angleY = rotY;
        this.angleZ = rotZ;
        this.computeTransform();
    }

    private void computeTransform() {
        this.transform = new AffineTransform().rotateX(angleX).rotateY(angleY).rotateZ(angleZ);
    }

    @Override
    public void encode(ECOutputStream stream) throws IOException {
        stream.writeDouble(angleX);
        stream.writeDouble(angleY);
        stream.writeDouble(angleZ);
    }

    @Override
    public void decode(ECInputStream stream) throws IOException {
        this.angleX = stream.readDouble();
        this.angleY = stream.readDouble();
        this.angleZ = stream.readDouble();
        this.computeTransform();
    }

    @Override
    public List<BlockVector> apply(BlockVector pos) {
        List<BlockVector> positions = new LinkedList<>();
        positions.add(this.transform.apply(pos).round().toBlockVector());
        return positions;
    }
}
