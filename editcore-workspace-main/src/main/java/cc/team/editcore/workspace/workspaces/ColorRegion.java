package cc.team.editcore.workspace.workspaces;

import cc.team.editcore.network.ECInputStream;
import cc.team.editcore.network.ECOutputStream;
import cc.team.editcore.network.ECSerializable;

import java.io.IOException;

/**
 * A colored region, this will be displayed on the client side
 */
public class ColorRegion implements ECSerializable {

    @Override
    public void encodeInto(ECOutputStream stream) throws IOException {

    }

    @Override
    public void decodeFrom(ECInputStream stream) throws IOException {

    }
}
